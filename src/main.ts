import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const port = process.env.SERVER_PORT || 3000;

  await app.listen(3000, () => {
    Logger.log(`Server running on port: ${port}`, 'APP');
  });
}
bootstrap();
